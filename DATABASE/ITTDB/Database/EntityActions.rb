require_relative '../Database/FileActions';
class EntityActions
    @@objectArray = Array.new();

    def addObject(object)
      fileActions = FileActions.new();
      fileActions.createDirectory;
      fileActions.createFile(object);
      filePath = fileActions.getPath(object);
      if (!File.empty?(filePath))
        @@objectArray = fileActions.read(object);
      end
      object.id = (@@objectArray.size + 1);
      @@objectArray.push(object);
      fileActions.write(filePath, @@objectArray);
    end
end