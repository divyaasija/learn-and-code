class FileActions

  def getPath(object)
    path = "output/" + object.class.name + ".json"
    return path;
  end

  def read(object)
    path = getPath(object)
    readList = nil
    File.open(path, "r") {|listObject| readList = Marshal.load(listObject)}
    return readList
  end

  def write(path, list)
    File.open(path, "w") do |object|
      Marshal.dump(list, object)
    end
  end

  def createDirectory
    begin
      if (!File.directory?("output"))
        directory = File.absolute_path("output")
        FileUtils.mkdir directory
      end
    rescue
      puts "Unable to create directory"
    end
  end

  def createFile(object)
    path = getPath(object)
    begin
      if (!File.file?(path))
        puts "not exists"
        File.new("output/" + object.class.name + ".json", "w+")
      end
    rescue
      puts "Unable to create file #{object.class.name} .json"
    end
  end

end