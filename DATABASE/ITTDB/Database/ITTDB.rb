require 'fileutils';
require_relative 'FileActions';
require_relative '../Database/EntityActions';
class ITTDB

  @@fileActions = FileActions.new();
  @@objectArray = Array.new();

  def save (object)
    entityActions = EntityActions.new();
    entityActions.addObject(object);
  end

  def find(object, searchKey, searchValue)
    @@objectArray = @@fileActions.read(object);
    searchList = @@objectArray.find_all {|tempObject| tempObject.instance_variable_defined?(searchKey) and tempObject.instance_variable_get(searchKey) == searchValue};
    if (searchList.empty?)
      return "no data found";
    else
      return searchList;
    end
  end

  def delete(object, deleteKey, deleteValue)
    affectedRows = 0
    filePath = @@fileActions.getPath(object);
    @@objectArray = @@fileActions.read(object);
    deleteObject = @@objectArray.find {|tempObject| tempObject.instance_variable_defined?(deleteKey) and tempObject.instance_variable_get(deleteKey) == deleteValue};

    if (deleteObject != nil)
      @@objectArray.delete(deleteObject);
      @@fileActions.write(filePath, @@objectArray);
      affectedRows += 1;
    end
    return affectedRows

  end

  def update(object, id, key, value)
    affectedRows = 0;
    filePath = @@fileActions.getPath(object);
    @@objectArray = @@fileActions.read(object);
    searchObject = find(object, "@id", id);
    if (!searchObject.empty?)
      searchObject.instance_variable_set(key, value)
      @@objectArray.delete_at(id - 1);
      @@objectArray.insert(id - 1, searchObject)
      @@fileActions.write(filePath, @@objectArray)
      affectedRows += 1;
    end
    return affectedRows;
  end

end