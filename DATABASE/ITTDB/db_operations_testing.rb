require 'test/unit';
require_relative 'Database/ITTDB';
require_relative 'GetSetClasses/Student';
require_relative 'GetSetClasses/Employee';
require_relative 'Database/FileActions';

class DbOperationsTesting < Test::Unit::TestCase
  @@student = Array.new();
  @@ittdb = ITTDB.new();
  @@student1 = Student.new("A", "B");
  @@student2 = Student.new("X", "y");
  @@employee1 = Employee.new("A", "B");
  @@employee2 = Employee.new("X", "y");
  @@ittdb.save(@@student1);
  @@ittdb.save(@@student2);
  @@ittdb.save(@@employee1);
  @@ittdb.save(@@employee2);

  def testFindFunction
    expectedStudent = Student.new("A", "B");
    expectedStudent.id = 1;
    @@student = @@ittdb.find(expectedStudent, '@name', "A");
    assert_equal(expectedStudent.name, @@student[0].name);
  end

  def testDeleteFunction
    returnStatus = @@ittdb.delete(@@student1, "@name", "A")
    assert_equal(1, returnStatus,returnStatus);
  end

  def testUpdateFunction
    returnStatus = @@ittdb.update(@@employee1, 1, "@name", "abc")
    assert_equal(1, returnStatus);
  end
end