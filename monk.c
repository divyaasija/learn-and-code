#include<stdio.h>
 int maximumPowerIndex,index,pushIndex,numberOfSpiders,decrementValue,finalQueueLength,numberOfSelectedSpiders,updatedPowerArray[10000000],updatedIndexArray[10000000],maximumPower,indexArray[10000000],powerArray[10000000],powerIndex,spiderIndex;

void updatePowerArray(int indexOfSpider){
    
 for(pushIndex=1;indexOfSpider<numberOfSpiders;indexOfSpider++)
        {
            
        powerArray[indexOfSpider]=updatedPowerArray[pushIndex];
        indexArray[indexOfSpider]=updatedIndexArray[pushIndex++];
        }
}

int settingQueueLength(){
    if(numberOfSpiders<numberOfSelectedSpiders){
            finalQueueLength=numberOfSpiders;
        }
        else{
            finalQueueLength=numberOfSelectedSpiders;
        }
        return finalQueueLength;
}
void maxPowerSpider(int queueLength){
    for(index=1;index<=queueLength;index++){
            if(powerArray[index]>maximumPower)
            {
            maximumPower=powerArray[index];
            maximumPowerIndex=indexArray[index];
            }
        }
}
void updatePower(){
    for(index=1;index<=numberOfSelectedSpiders;index++)
        {
            if(maximumPowerIndex==indexArray[index])
            {
            decrementValue=1;
            continue;
            }
            //decrementing power of spider
            if(powerArray[index]>0){
            updatedPowerArray[index-decrementValue]=powerArray[index]-1;
            }
            else{
            updatedPowerArray[index-decrementValue]=powerArray[index];
            }
            updatedIndexArray[index-decrementValue]=indexArray[index];
        }
}

int enqueuePower(){
    for(index=1;index<=numberOfSpiders-numberOfSelectedSpiders;index++)
        {
            powerArray[index]=powerArray[index+numberOfSelectedSpiders];
            indexArray[index]=indexArray[index+numberOfSelectedSpiders];
        }
        return index;
}
int main()
{
   
    scanf("%d%d",&numberOfSpiders,&numberOfSelectedSpiders);
    //enter power of every spider and stores index in new array
    for(powerIndex=1;powerIndex<=numberOfSpiders;powerIndex++)
    {
        scanf("%d",&powerArray[powerIndex]);
        indexArray[powerIndex]=powerIndex;
    }

    //dequeuing spiders and decrementing power
    for(spiderIndex=1;spiderIndex<=numberOfSelectedSpiders;spiderIndex++)
    {
        maximumPower=-1;

        //setting array size according to input or left spiders
        int queueLength=settingQueueLength();

        //finding spider with maximum power
        maxPowerSpider(queueLength);

        decrementValue=0;

        //update power of each spider
        updatePower();
        
        //enqueueing the spider power after decrement
        int spiderIndex=enqueuePower();

        //updating values in array
       updatePowerArray(spiderIndex);

        numberOfSpiders--;
        printf("%d ",maximumPowerIndex);
        }
    return 0;
}

